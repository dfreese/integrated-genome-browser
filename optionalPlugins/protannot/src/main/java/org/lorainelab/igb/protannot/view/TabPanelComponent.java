/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.protannot.view;

import java.awt.Component;

/**
 *
 * @author jeckstei
 */
public interface TabPanelComponent {
    public Component getComponent();
    public String getName();
}
