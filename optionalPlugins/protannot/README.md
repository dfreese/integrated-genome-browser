ProtAnnot is a protein annotation viewer implemented in Java that displays  
protein annotations in the context of the genomic sequence, making it easy  
to see how alternative mRNA structures affect the various protein products  
encoded at a locus.  

ProtAnnot is developed in the Loraine Lab, part of the Bioinformatics and  
Genomics Department at University of North Carolina at Charlotte. 

#### **Starting ProtAnnot**
Once installed, you can open ProtAnnot in IGB by selecting one or more gene  
models on the same strand of DNA and selecting **Tools > Start ProtAnnot**.
